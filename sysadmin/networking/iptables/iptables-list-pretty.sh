#!/usr/bin/env bash

#
# Pretty-print iptables(8) output.
#
# source:
# https://gitlab.com/johnnyutahh/swmisc/-/blob/master/sysadmin/networking/iptables/iptables-list-pretty.sh
#
# (The following script was Ubuntu-18.04 tested on 2020-05-17.)
#

# Derivered from
# https://www.reddit.com/r/bash/comments/gl61yb/selectively_remove_unnecessary_column_whitespace/fqw19tv

# Adjust these values to resize column widths
column_widths=(0 0 27 5 4 8 8 17 17)

iptables_align()
{
  while read line; do
    if [[ $line =~ Chain ]]; then
      echo "$line"
    else
      line=${line//\*/\\\*}
      array=($line)
      for n in {2..8}; do
        w=${column_widths[$n]}
         printf "%-${w}s" "${array[$n]}"
      done
      lastcol_with_spaces_in_content=("${array[@]:9}")
      printf "%s" "${lastcol_with_spaces_in_content[*]}"
      echo
    fi
  done
}

iptables -nvL | iptables_align | sed -s 's|\\\*|* |g' | less
